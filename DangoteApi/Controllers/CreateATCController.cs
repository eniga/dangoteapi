﻿using DangoteApi.Models;
using DangoteApi.Services;
using DangoteApi.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Security;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace DangoteApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("api/createatc")]
    public class CreateATCController : ApiController
    {
        Serilog.Core.Logger logger = Helper.LogConfig();
        TransactionService service = new TransactionService();

        [HttpPost]
        public async Task<DangoteATCService.si_atm_dp_bapi_abs_syncResponse> CreateATCPayment([FromBody]CreateATCPayload request)
        {
            var result = new DangoteATCService.si_atm_dp_bapi_abs_syncResponse();
            //var result = new DangoteATCService.dt_doc_out();
            string Id = request.tran_id;

            try
            {
                //Attempting to save transaction details to DB
                //Create db object
                var context = new TransactionsContext()
                {
                    amount = request.amount,
                    teller_no = request.teller_no,
                    atc_doc_number = null,
                    barcode1 = null,
                    barcode2 = null,
                    comp_code = request.comp_code,
                    receiver_name = request.receiver_name,
                    doc_num = null,
                    fiscal_yr = null,
                    order_status = "Pending",
                    receiver_address = request.dest_address,
                    receiver_country = request.dest_country,
                    receiver_region = request.dest_region,
                    status = "Pending",
                    transaction_date = DateTime.Now,
                    transaction_type = "ATC",
                    tran_id = request.tran_id,
                    username = request.username,
                    processed_date = DateTime.Now,
                    cust_name = request.cust_name
                };

                var insert = service.Add(context);

                logger.Information("Dangote createatc:: request - " + Helper.ObjectToJson(request));

                // Debit customer account on flexcube
                Flexcube.DEResponse DEResponse = Flex.PostEntry(request.account_no, $"Dangote e-Wallet Request for {request.cust_name}", decimal.Parse(request.amount));
                if (DEResponse.Errors.Count > 0)
                {
                    context.status = "Failed";
                    context.order_status = DEResponse.Message;
                    service.Update(context);
                    result.mt_doc_out.error.Append(new DangoteATCService.dt_doc_outLines4()
                    {
                        code = DEResponse.ResponseCode.ToString(),
                        msg = DEResponse.Message,
                        title = DEResponse.Errors[0]
                    });
                    return result;
                }

                context.teller_no = DEResponse.Reference;
                service.Update(context);

                DangoteATCService.si_atm_dp_bapi_abs_syncClient client = new DangoteATCService.si_atm_dp_bapi_abs_syncClient();

                string token = Auth.GetToken();

                DangoteATCService.dt_atm_dp_in req = new DangoteATCService.dt_atm_dp_in()
                {
                    sales_org = request.sales_org,
                    cust_num = request.cust_num,
                    comp_code = request.comp_code,
                    access_token = token,
                    amount = request.amount,
                    bankn = request.bankn,
                    currency = request.currency,
                    delv_status = request.delv_status,
                    dest_address = request.dest_address,
                    dest_city = request.dest_city,
                    dest_country = request.dest_country,
                    dest_region = request.dest_region,
                    //export = request.export,
                    material = request.material,
                    order_amount = request.order_amount,
                    plant = request.plant,
                    quantity = request.quantity,
                    receiver_name = request.receiver_name,
                    split_quantity = request.split_quantity,
                    split_type= request.split_type,
                    teller_no = request.teller_no,
                    tran_id = request.tran_id
                };

                // Set credentials
                string baseAuthusername = ConfigurationManager.AppSettings["baseAuthusername"];
                string baseAuthpassword = ConfigurationManager.AppSettings["baseAuthpassword"];

                client.ClientCredentials.UserName.UserName = baseAuthusername;
                client.ClientCredentials.UserName.Password = baseAuthpassword;
                client.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.None;


                using (OperationContextScope scope = new OperationContextScope(client.InnerChannel))
                {
                    var httpRequestProperty = new HttpRequestMessageProperty();
                    httpRequestProperty.Headers[System.Net.HttpRequestHeader.Authorization] = "Basic " +
                                 Convert.ToBase64String(Encoding.ASCII.GetBytes(client.ClientCredentials.UserName.UserName + ":" +
                                 client.ClientCredentials.UserName.Password));

                    OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = httpRequestProperty;

                    DangoteATCService.dt_doc_out syncResponse =  client.si_atm_dp_bapi_abs_sync(req);
                    result.mt_doc_out = syncResponse;


                }
                

                if(result.mt_doc_out.error.Length < 1)
                {
                    var atc_doc_numbers = result.mt_doc_out.payment.Select(x => x.document);
                    var doc_nums = result.mt_doc_out.parent.Select(x => x.docnum);
                    var order_statuses = result.mt_doc_out.parent.Select(x => x.info);
                    context.doc_num = string.Join(",", doc_nums);
                    context.atc_doc_number = string.Join(",", atc_doc_numbers);
                    context.order_status = order_statuses.FirstOrDefault();
                    context.barcode1 = result.mt_doc_out.barcode_string1;
                    context.barcode2 = result.mt_doc_out.barcode_string2;
                    context.fiscal_yr = result.mt_doc_out.payment[0].fiscal_yr;
                    context.status = "Successful";
                    result.mt_doc_out.access_token = context.teller_no;
                }
                else
                {
                    context.order_status = result.mt_doc_out.error[0].msg;
                    context.status = "Failed";
                    context.processed_date = DateTime.Now;
                }
                var update = service.Update(context);
                //result = service.si_atm_dp_bapi_abs_sync(request);
                logger.Information("Dangote createatc:: response; - " + Helper.ObjectToJson(result));
            }
            catch (Exception ex)
            {
                logger.Error("Dangote createatc:: " + ex.Message);
                var context = service.GetByIdAsync(Id);
                context.status = "Failed";
                var update = service.Update(context);
            }
            return result;
        }
    }
}