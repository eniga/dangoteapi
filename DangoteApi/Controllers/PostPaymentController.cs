﻿using DangoteApi.Models;
using DangoteApi.Services;
using DangoteApi.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Security;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace DangoteApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("api/postpayment")]
    public class PostPaymentController : ApiController
    {
        Serilog.Core.Logger logger = Helper.LogConfig();
        TransactionService service = new TransactionService();

        [HttpPost]
        public async Task<DangotePostPaymentService.si_bank_abs_syc_v2Response> PostPayment([FromBody]PostPaymentPayload request)
        {
            var result = new DangotePostPaymentService.si_bank_abs_syc_v2Response();
            DangotePostPaymentService.dt_bank_out resultSync = new DangotePostPaymentService.dt_bank_out();

            string Id = request.tran_id;
            try
            {
                //Attempting to save transaction details to DB
                //Create db object
                var context = new TransactionsContext()
                {
                    amount = request.amount,
                    teller_no = request.teller_no,
                    atc_doc_number = null,
                    barcode1 = null,
                    barcode2 = null,
                    comp_code = request.comp_code,
                    receiver_name = null,
                    doc_num = null,
                    fiscal_yr = null,
                    order_status = "Pending",
                    receiver_address = null,
                    receiver_country = null,
                    receiver_region = null,
                    status = "Pending",
                    transaction_date = DateTime.Now,
                    transaction_type = "eWallet",
                    tran_id = request.tran_id,
                    username = request.username,
                    processed_date = DateTime.Now,
                    cust_name = request.cust_name
                };

                var insert = service.Add(context);

                logger.Information("Dangote postpayment:: request - " + Helper.ObjectToJson(request));

                // Debit customer account on flexcube
                Flexcube.DEResponse DEResponse = Flex.PostEntry(request.account_no, $"Dangote e-Wallet Request for {request.cust_name}", decimal.Parse(request.amount));
                if(DEResponse.Errors.Count > 0)
                {
                    context.status = "Failed";
                    context.order_status = DEResponse.Message;
                    service.Update(context);
                    result.mt_bank_out.ERROR.Append(new DangotePostPaymentService.dt_bank_outLines()
                    {
                        code = DEResponse.ResponseCode.ToString(),
                        msg = DEResponse.Message,
                        title = DEResponse.Errors[0]
                    });
                    return result;
                }

                context.teller_no = DEResponse.Reference;
                service.Update(context);

                DangotePostPaymentService.si_bank_abs_syc_v2Client client = new DangotePostPaymentService.si_bank_abs_syc_v2Client();

                string token = Auth.GetToken();
                request.access_token = token;

                DangotePostPaymentService.dt_bank_in req = new DangotePostPaymentService.dt_bank_in()
                {
                    tran_id = request.tran_id,
                    teller_no = request.teller_no,
                    plant = request.plant,
                    access_token = token,
                    amount = request.amount,
                    bankn = request.bankn,
                    comp_code = request.comp_code,
                    currency = request.currency,
                    cust_number = request.cust_number,
                    //Export =request.Export
                };
                
                // Set credentials
                string baseAuthusername = ConfigurationManager.AppSettings["baseAuthusername"];
                string baseAuthpassword = ConfigurationManager.AppSettings["baseAuthpassword"];

                client.ClientCredentials.UserName.UserName = baseAuthusername;
                client.ClientCredentials.UserName.Password = baseAuthpassword;
                client.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.None;

                using (OperationContextScope scope = new OperationContextScope(client.InnerChannel))
                {
                    var httpRequestProperty = new HttpRequestMessageProperty();
                    httpRequestProperty.Headers[System.Net.HttpRequestHeader.Authorization] = "Basic " +
                                 Convert.ToBase64String(Encoding.ASCII.GetBytes(client.ClientCredentials.UserName.UserName + ":" +
                                 client.ClientCredentials.UserName.Password));

                    OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = httpRequestProperty;
                    //result = await client.si_bank_abs_syc_v2Async(req);
                    resultSync =  client.si_bank_abs_syc_v2(req);
                    result.mt_bank_out = resultSync;
                }

                

                if (result.mt_bank_out.ERROR == null)
                {
                    context.doc_num = result.mt_bank_out.DocNum;
                    context.barcode1 = result.mt_bank_out.output_string1;
                    context.barcode2 = result.mt_bank_out.output_string2;
                    context.fiscal_yr = result.mt_bank_out.FiscalYr;
                    context.status = "Successful";
                    context.order_status = context.status;
                }
                else
                {
                    context.order_status = result.mt_bank_out.ERROR[0].msg;
                    context.status = "Failed";
                    context.processed_date = DateTime.Now;
                }
                var update = service.Update(context);

                logger.Information("Dangote postpayment:: response - " + Helper.ObjectToJson(result));
            }
            catch (Exception ex)
            {
                logger.Error("Dangote postpayment:: " + ex.Message);
                var context = service.GetByIdAsync(Id);
                context.status = "Failed";
                var update = service.Update(context);
            }
            return result;
        }
    }
}