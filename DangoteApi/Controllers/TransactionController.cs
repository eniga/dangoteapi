﻿using DangoteApi.Models;
using DangoteApi.Services;
using DangoteApi.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace DangoteApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("api/transaction")]
    public class TransactionController : ApiController
    {
        Serilog.Core.Logger logger = Helper.LogConfig();
        TransactionService service = new TransactionService();

        // GET api/<controller>
        [HttpGet]
        public IEnumerable<TransactionsContext> ListAsync()
        {
            return service.GetAllAsync();
        }

        [HttpGet]
        [Route("username/{username}")]
        public IEnumerable<TransactionsContext> ListUserAsync(string username)
        {
            return service.GetAllUserAsync(username);
        }

        // GET api/<controller>/5
        [HttpGet]
        [Route("{tran_id}")]
        public TransactionsContext Get(string tran_id)
        {
            return service.GetByIdAsync(tran_id);
        }
    }
}