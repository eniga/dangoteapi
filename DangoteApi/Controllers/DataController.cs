﻿using DangoteApi.Models;
using DangoteApi.Services;
using DangoteApi.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace DangoteApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("api/data")]
    public class DataController : ApiController
    {
        Serilog.Core.Logger logger = Helper.LogConfig();

        [HttpGet]
        public DataContext GetData()
        {
            return new DataContext();
        }

        [HttpGet]
        [Route("areas")]
        public async Task<IEnumerable<AreasContext>> GetAreas()
        {
            AreaService service = new AreaService();
            try
            {
                return await service.GetAllAsync();
            }
            catch (Exception ex)
            {
                logger.Error("Dangote areas:: " + ex.Message);
                throw new Exception(ex.Message);
            }
        }

        [HttpGet]
        [Route("records")]
        public async Task<IEnumerable<RecordsContext>> GetRecords()
        {
            RecordService service = new RecordService();
            try
            {
                return await service.GetAllAsync();
            }
            catch (Exception ex)
            {
                logger.Error("Dangote areas:: " + ex.Message);
                throw new Exception(ex.Message);
            }
        }

        [HttpGet]
        [Route("regions")]
        public async Task<IEnumerable<RegionsContext>> GetRegions()
        {
            RegionService service = new RegionService();
            try
            {
                return await service.GetAllAsync();
            }
            catch (Exception ex)
            {
                logger.Error("Dangote areas:: " + ex.Message);
                throw new Exception(ex.Message);
            }
        }
    }
}