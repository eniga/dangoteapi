﻿using DangoteApi.Flexcube;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace DangoteApi.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ValuesController : ApiController
    {
        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            Flexcube.DEServiceSoapClient client = new Flexcube.DEServiceSoapClient();
            DEResponse returnValue = client.PostDE("PROCESSMAKER", "PROCESSMAKER", string.Empty, "1000087712",
                "1000063370", "NGN", "117", "NARRATION", "DESC", new DateTime(2020, 05, 29), 100);

            return string.Empty;
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
