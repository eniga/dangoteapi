﻿using DangoteApi.Models;
using DangoteApi.Services;
using DangoteApi.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Security;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace DangoteApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("api/customerdetails")]
    public class GetCustomerController : ApiController
    {
        Serilog.Core.Logger logger = Helper.LogConfig();
        CustomerService service = new CustomerService();

        [HttpPost]
        public async Task<DangoteGetCustomerService.si_atm_cust_abs_syncResponse> GetCustomerDetails([FromBody]GetcustomrRequest request)
        {
            // Temporary workaround
            // I added this workaround because the Angular app was behaving unpredictably when i returned the response
            // from the synchronous call, so i still make a synchrounous call and map the response to the return parameter
            // for the async call. I coulnd't get the source code for the angular app to see what the problem may be.

            var resultAsync = new DangoteGetCustomerService.si_atm_cust_abs_syncResponse();
            var result = new DangoteGetCustomerService.dt_atm_cust_out();

            int Id = 0;
            try
            {
                // Set config parameters
                var DangoteCompanyCode = ConfigurationManager.AppSettings["DangoteCompanyCode"];
                var DangoteCreditArea = ConfigurationManager.AppSettings["DangoteCreditArea"];
                var DangoteSalesOrg = ConfigurationManager.AppSettings["DangoteSalesOrg"];
                //Attempting to save transaction details to DB
                //Create db object
                var context = new CustomerContext()
                {
                    cc_area = DangoteCreditArea,
                    comp_code = DangoteCompanyCode,
                    cust_num = request.cust_num,
                    customer_address = null,
                    access_token = null,
                    customer_country = null,
                    customer_name = null,
                    customer_region = null,
                    sales_org = DangoteSalesOrg,
                    username = request.username,
                    status = "Pending",
                    request_date = DateTime.Now
                };

                var insert = service.Add(context);
                Id = context.Id;

                logger.Information("Dangote Customer details:: request - " + Helper.ObjectToJson(request));
                DangoteGetCustomerService.si_atm_cust_abs_syncClient client = new DangoteGetCustomerService.si_atm_cust_abs_syncClient();

                string token = Auth.GetToken();

                DangoteGetCustomerService.dt_atm_cust_in req = new DangoteGetCustomerService.dt_atm_cust_in()
                {
                    access_token = token,
                    cc_area = DangoteCreditArea,
                    comp_code = DangoteCompanyCode,
                    cust_num = request.cust_num,
                    sales_org = DangoteSalesOrg
                };
                // Set credentials
                string baseAuthusername = ConfigurationManager.AppSettings["baseAuthusername"];
                string baseAuthpassword = ConfigurationManager.AppSettings["baseAuthpassword"];

                client.ClientCredentials.UserName.UserName = baseAuthusername;
                client.ClientCredentials.UserName.Password = baseAuthpassword;
                client.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.None;
                
                using (OperationContextScope scope = new OperationContextScope(client.InnerChannel))
                {
                    var httpRequestProperty = new HttpRequestMessageProperty();
                    httpRequestProperty.Headers[System.Net.HttpRequestHeader.Authorization] = "Basic " +
                                 Convert.ToBase64String(Encoding.ASCII.GetBytes(client.ClientCredentials.UserName.UserName + ":" +
                                 client.ClientCredentials.UserName.Password));

                    OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = httpRequestProperty;
                    result =  client.si_atm_cust_abs_sync(req);
                    //DangoteGetCustomerService.dt_atm_cust_out x =  client.si_atm_cust_abs_sync(req);
                }

                    
                
                if (result.error_log.Length < 1)
                {
                    context.customer_address = result.address;
                    context.customer_country = result.country;
                    context.customer_name = result.cust_name;
                    context.customer_region = result.region;
                    context.status = "Successful";

                    // Part of workaround
                    resultAsync.mt_atm_cust_out = new DangoteGetCustomerService.dt_atm_cust_out();
                    resultAsync.mt_atm_cust_out.access_token = result.access_token ?? null;
                    resultAsync.mt_atm_cust_out.address = result.address ?? null;
                    resultAsync.mt_atm_cust_out.country = result.country ?? null;
                    resultAsync.mt_atm_cust_out.region = result.region ?? null;
                    resultAsync.mt_atm_cust_out.cust_name = result.cust_name ?? null;
                    resultAsync.mt_atm_cust_out.division = result.division ?? null;
                    resultAsync.mt_atm_cust_out.exposure = result.exposure ?? null;
                    //end workaround
                }
                else
                {
                    context.status = "Failed";
                }
                var update = service.Update(context);

                logger.Information("Dangote Customer details:: response - " + Helper.ObjectToJson(result));
            }
            catch (Exception ex)
            {
                logger.Error("Dangote Customer details:: " + ex.Message);
                var context = service.GetById(Id);
                context.status = "Failed";
                var update = service.Update(context);
            }
            return resultAsync;
        }
    }
}