﻿using DangoteApi.Models;
using DangoteApi.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Security;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace DangoteApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("api/getprice")]
    public class GetPriceController : ApiController
    {
        Serilog.Core.Logger logger = Helper.LogConfig();

        [HttpPost]
        public async Task<DangotePriceService.si_price_abs_syncResponse> GetPrice([FromBody]GetPricePayload request)
        {
            var result = new DangotePriceService.si_price_abs_syncResponse();
            DangotePriceService.dt_price_out resultSync = new DangotePriceService.dt_price_out();

            logger.Information("Dangote getprice:: request - " + Helper.ObjectToJson(request));
            DangotePriceService.si_price_abs_syncClient service = new DangotePriceService.si_price_abs_syncClient();
            try
            {
                // Set credentials
                string baseAuthusername = ConfigurationManager.AppSettings["baseAuthusername"];
                string baseAuthpassword = ConfigurationManager.AppSettings["baseAuthpassword"];

                service.ClientCredentials.UserName.UserName = baseAuthusername;
                service.ClientCredentials.UserName.Password = baseAuthpassword;
                service.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.None;

                string token = Auth.GetToken();
                request.acess_token = token;

                DangotePriceService.dt_price_in req = new DangotePriceService.dt_price_in()
                {
                    acess_token = token,
                    city = request.city,
                    country = request.country,
                    cust_name = request.cust_name,
                    cust_num = request.cust_num,
                    region = request.region,
                    delv_status = request.delv_status,
                    division = request.division,
                    //export = request.export,
                    materials = request.materials,
                    plant = request.plant,
                    quantities = request.quantities,
                    sales_org = request.sales_org,
                    street = request.street,
                    tran_id = request.tran_id
                };

                using (OperationContextScope scope = new OperationContextScope(service.InnerChannel))
                {
                    var httpRequestProperty = new HttpRequestMessageProperty();
                    httpRequestProperty.Headers[System.Net.HttpRequestHeader.Authorization] = "Basic " +
                                 Convert.ToBase64String(Encoding.ASCII.GetBytes(service.ClientCredentials.UserName.UserName + ":" +
                                 service.ClientCredentials.UserName.Password));

                    OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = httpRequestProperty;
                    //result = await service.si_price_abs_syncAsync(req);
                    resultSync = service.si_price_abs_sync(req);
                    //DangoteGetCustomerService.dt_atm_cust_out x =  client.si_atm_cust_abs_sync(req);
                    if(resultSync != null)
                    {
                        result.mt_price_out = resultSync;
                    }
                }
                
                logger.Information("Dangote getprice:: response - " + Helper.ObjectToJson(result));
            }
            catch (Exception ex)
            {
                logger.Error("Dangote getprice:: " + ex.Message);
            }
            return result;
        }
    }
}