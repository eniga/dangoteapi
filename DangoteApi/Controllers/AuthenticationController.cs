﻿using DangoteApi.Models;
using DangoteApi.Utilities;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.ServiceModel.Security;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace DangoteApi.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("api/authentication")]
    public class AuthenticationController : ApiController
    {
        Serilog.Core.Logger logger = Helper.LogConfig();

        [HttpGet]
        public DangoteAuthService.si_full_auth_absResponse Authentication()
        {
            var result = new DangoteAuthService.si_full_auth_absResponse();
            try
            {
                result = Auth.Authentication();
            }
            catch (Exception ex)
            {
                logger.Error("Dangote Authentication:: " + ex.Message);
                throw new Exception(ex.Message);
            }
            return result;
        }

        [HttpGet]
        [Route("token")]
        public string GetToken()
        {
            string token = string.Empty;
            try
            {
                token = Auth.GetToken();
            }
            catch (Exception ex)
            {
                logger.Error("Dangote GetToken:: " + ex.Message);
                throw new Exception(ex.Message);
            }
            return token;
        }

        [HttpPost]
        [Route("passwordreset")]
        public async Task<DangotePasswordResetService.si_pass_reset_absResponse> PasswordReset([FromBody]DangotePasswordResetService.dt_pass_rest_in request)
        {
            logger.Information("Dangote Authentication:: request - " + Helper.ObjectToJson(request));
            DangotePasswordResetService.si_pass_reset_absClient service = new DangotePasswordResetService.si_pass_reset_absClient();

            // Set credentials
            string baseAuthusername = ConfigurationManager.AppSettings["baseAuthusername"];
            string baseAuthpassword = ConfigurationManager.AppSettings["baseAuthpassword"];

            service.ClientCredentials.UserName.UserName = baseAuthusername;
            service.ClientCredentials.UserName.Password = baseAuthpassword;
            service.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.None;

            var result = await service.si_pass_reset_absAsync(request);
            logger.Information("Dangote Authentication:: response - " + Helper.ObjectToJson(result));
            return result;
        }

        [HttpPost]
        [Route("login")]
        public async Task<Object> Login([FromBody]LoginRequest request)
        {
            var response = new Object();
            try
            {
                logger.Information("Dangote Login:: process begin for " + request.username);
                string ADApi = ConfigurationManager.AppSettings["ADApi"];
                RestClient client = new RestClient(ADApi);
                RestRequest req = new RestRequest(ADApi, method: Method.POST);
                req.AddJsonBody(request);
                var result = client.Execute(req);
                logger.Information("Dangote Login:: response - " + Helper.ObjectToJson(result.Content));
                if (result.IsSuccessful)
                {
                    response = JsonConvert.DeserializeObject<Object>(result.Content);
                }
            }
            catch (Exception ex)
            {
                logger.Error("Dangote Login:: " + ex.Message);
            }
            return response;
        }
    }
}