﻿using DangoteApi.Flexcube;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace DangoteApi.Utilities
{
    public static class Flex
    {
        static Serilog.Core.Logger logger = Helper.LogConfig();
        public static DEResponse PostEntry(string debitAccount, string narration, decimal amount)
        {
            DEResponse response = new DEResponse();
            try
            {
                // Set flexcube parameters
                string FlexcubeSource = ConfigurationManager.AppSettings["FlexcubeSource"];
                string FlexcubeUserId = ConfigurationManager.AppSettings["FlexcubeUserId"];
                string FlexcubeCreditAccount = ConfigurationManager.AppSettings["FlexcubeCreditAccount"];
                string FlexcubeCurrency = ConfigurationManager.AppSettings["FlexcubeCurrency"];
                string FlexcubeTranCode = ConfigurationManager.AppSettings["FlexcubeTranCode"];

                DEServiceSoapClient client = new DEServiceSoapClient();
                response = client.PostDE(FlexcubeSource, FlexcubeUserId, string.Empty, debitAccount,
                    FlexcubeCreditAccount, FlexcubeCurrency, FlexcubeTranCode, narration, narration, new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day), amount);
            }
            catch (Exception ex)
            {
                logger.Error($"Attempt to debit {debitAccount} failed with error message {ex.Message}");
                response.ResponseCode = 99;
                response.Errors.Add("System Malfunction");
            }
            return response;
        }
    }
}