﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Security;
using System.Text;
using System.Web;

namespace DangoteApi.Utilities
{
    public class Auth
    {
        static Serilog.Core.Logger logger = Helper.LogConfig();

        public static DangoteAuthService.si_full_auth_absResponse Authentication()
        {
            var result = new DangoteAuthService.si_full_auth_absResponse();
            try
            {
                string username = ConfigurationManager.AppSettings["username"];
                string password = ConfigurationManager.AppSettings["password"];

                DangoteAuthService.dt_full_auth_in request = new DangoteAuthService.dt_full_auth_in()
                {
                    full_synch = null,
                    password = password,
                    username = username
                };
                logger.Information("Dangote Authentication:: request - " + Helper.ObjectToXml(request));
                DangoteAuthService.si_full_auth_absClient service = new DangoteAuthService.si_full_auth_absClient();
                // Set credentials
                string baseAuthusername = ConfigurationManager.AppSettings["baseAuthusername"];
                string baseAuthpassword = ConfigurationManager.AppSettings["baseAuthpassword"];

                service.ClientCredentials.UserName.UserName = baseAuthusername;
                service.ClientCredentials.UserName.Password = baseAuthpassword;
                service.ClientCredentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.None;

                // damilola's edit
                using (OperationContextScope scope = new OperationContextScope(service.InnerChannel))
                {
                    // Damilola's edit
                    var httpRequestProperty = new HttpRequestMessageProperty();
                    httpRequestProperty.Headers[System.Net.HttpRequestHeader.Authorization] = "Basic " +
                                 Convert.ToBase64String(Encoding.ASCII.GetBytes(service.ClientCredentials.UserName.UserName + ":" +
                                 service.ClientCredentials.UserName.Password));

                    OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = httpRequestProperty;
                    // end of damilola's edit.


                    result = service.si_full_auth_absAsync(request).GetAwaiter().GetResult();
                    logger.Information("Dangote Authentication:: response - " + Helper.ObjectToXml(result));
                }

                
            }
            catch (Exception ex)
            {
                logger.Error("Dangote Authentication:: " + ex.Message);
                throw new Exception(ex.Message);
            }
            return result;
        }

        public static string GetToken()
        {
            string token = string.Empty;
            try
            {
                var response = Authentication();
                if (response.GetType() == typeof(DangoteAuthService.si_full_auth_absResponse))
                {
                    var result = response.mt_full_auth_out;
                    if (result.error_log.Length < 1)
                        token = result.authtoken;
                }

            }
            catch (Exception ex)
            {
                logger.Error("Dangote GetToken:: " + ex.Message);
                throw new Exception(ex.Message);
            }
            return token;
        }
    }
}