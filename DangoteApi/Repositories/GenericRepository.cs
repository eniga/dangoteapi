﻿using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace DangoteApi.Repositories
{
    public class GenericRepository : DapperRepository<Object>
    {
        public GenericRepository(IDbConnection connection, ISqlGenerator<Object> sqlGenerator)
        : base(connection, sqlGenerator)
        {
        }
    }
}