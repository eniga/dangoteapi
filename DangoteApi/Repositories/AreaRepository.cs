﻿using DangoteApi.Models;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace DangoteApi.Repositories
{
    class AreaRepository : DapperRepository<AreasContext>
    {
        public AreaRepository(IDbConnection connection, ISqlGenerator<AreasContext> sqlGenerator)
        : base(connection, sqlGenerator)
        {
        }
    }
}