﻿using DangoteApi.Models;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace DangoteApi.Repositories
{
    class RegionRepository : DapperRepository<RegionsContext>
    {
        public RegionRepository(IDbConnection connection, ISqlGenerator<RegionsContext> sqlGenerator)
        : base(connection, sqlGenerator)
        {
        }
    }
}