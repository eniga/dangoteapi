﻿using DangoteApi.Models;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace DangoteApi.Repositories
{
    public class CustomerRepository : DapperRepository<CustomerContext>
    {
        public CustomerRepository(IDbConnection connection, ISqlGenerator<CustomerContext> sqlGenerator)
        : base(connection, sqlGenerator)
        {
        }
    }
}