﻿using DangoteApi.Models;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace DangoteApi.Repositories
{
    class RecordRepository : DapperRepository<RecordsContext>
    {
        public RecordRepository(IDbConnection connection, ISqlGenerator<RecordsContext> sqlGenerator)
        : base(connection, sqlGenerator)
        {
        }
    }
}