﻿using DangoteApi.Models;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace DangoteApi.Repositories
{
    public class PriceRepository : DapperRepository<GetPricePayload>
    {
        public PriceRepository(IDbConnection connection, ISqlGenerator<GetPricePayload> sqlGenerator)
        : base(connection, sqlGenerator)
        {
        }
    }
}