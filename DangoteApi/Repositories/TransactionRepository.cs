﻿using DangoteApi.Models;
using MicroOrm.Dapper.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace DangoteApi.Repositories
{
    public class TransactionRepository : DapperRepository<TransactionsContext>
    {
        public TransactionRepository(IDbConnection connection, ISqlGenerator<TransactionsContext> sqlGenerator)
        : base(connection, sqlGenerator)
        {
        }
    }
}