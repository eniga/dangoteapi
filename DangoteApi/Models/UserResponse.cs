﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DangoteApi.Models
{
    public class UserResponse : Response
    {
        public UserContext data { get; set; }
    }

    public class UserContext
    {
        public string StaffId { get; set; }
        public string Samaccount { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get => string.Format("{0} {1}", FirstName, LastName); }
    }

    public class Response
    {
        public bool Status { get; set; }
        public string Description { get; set; }
    }

    public class LoginRequest
    {
        public string username { get; set; }
        public string password { get; set; }
    }
}