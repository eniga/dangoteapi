﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DangoteApi.Models
{
    public class DEResponse
    {
        public int ResponseCode { get; set; }
        public string BatchNumber { get; set; }
        public string Reference { get; set; }
        public string Message { get; set; }
        public List<string> Warnings { get; set; }
        public List<string> Errors { get; set; }
    }
}