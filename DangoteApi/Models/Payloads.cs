﻿using MicroOrm.Dapper.Repositories.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.Web;

namespace DangoteApi.Models
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall, ConcurrencyMode = ConcurrencyMode.Reentrant)]
    public class GetCustomerPayload : DangoteGetCustomerService.dt_atm_cust_in
    {
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, Order = 0)]
        public string username { get; set; }
    }

    public class GetcustomrRequest
    {
        public string username { get; set; }
        public string cust_num { get; set; }
    }

    [Table("tbl_customer_log")]
    public class CustomerContext : GetCustomerPayload
    {
        [Key, Identity]
        public int Id { get; set; }
        public string customer_name { get; set; }
        public string customer_address { get; set; }
        public string customer_country { get; set; }
        public string customer_region { get; set; }
        public string status { get; set; }
        public DateTime request_date { get; set; }
    }

    [Table("tbl_getprice_log")]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall, ConcurrencyMode = ConcurrencyMode.Reentrant)]
    public class GetPricePayload : DangotePriceService.dt_price_in
    {
        public string username { get; set; }
    }

    [Table("tbl_postpayment_log")]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall, ConcurrencyMode = ConcurrencyMode.Reentrant)]
    public class PostPaymentPayload : DangotePostPaymentService.dt_bank_in
    {
        public string username { get; set; }
        public string cust_name { get; set; }
        public string account_no { get; set; }
    }

    [Table("tbl_atc_log")]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall, ConcurrencyMode = ConcurrencyMode.Reentrant)]
    public class CreateATCPayload : DangoteATCService.dt_atm_dp_in
    {
        public string username { get; set; }
        public string cust_name { get; set; }
        public string account_no { get; set; }
    }

    public class DataContext
    {
        public string cc_area { get => ConfigurationManager.AppSettings["DangoteCreditArea"]; }
        public string comp_code { get => ConfigurationManager.AppSettings["DangoteCompanyCode"]; }
        public string sales_org { get => ConfigurationManager.AppSettings["DangoteSalesOrg"]; }
    }

    public class ATCResponse : DangoteATCService.si_atm_dp_bapi_abs_syncResponse
    {
        public DEResponse FlexResponse { get; set; }
    }
}