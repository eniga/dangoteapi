﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DangoteApi.Models
{
    [Table("tbl_transactions")]
    public class TransactionsContext
    {
        [Key]
        public string tran_id { get; set; }
        public string cust_name { get; set; }
        public string receiver_name { get; set; }
        public string receiver_address { get; set; }
        public string receiver_country { get; set; }
        public string receiver_region { get; set; }
        public string teller_no { get; set; }
        public string comp_code { get; set; }
        public string doc_num { get; set; }
        public string atc_doc_number { get; set; }
        public string order_status { get; set; }
        public string fiscal_yr { get; set; }
        public string status { get; set; }
        public string amount { get; set; }
        public string transaction_type { get; set; }
        public DateTime transaction_date { get; set; }
        public DateTime processed_date { get; set; }
        public string barcode1 { get; set; }
        public string barcode2 { get; set; }
        public string username { get; set; }
        public string account_no { get; set; }
    }
}