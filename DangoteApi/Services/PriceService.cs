﻿using DangoteApi.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using MyRow = DangoteApi.Models.GetPricePayload;

namespace DangoteApi.Services
{
    public class PriceService
    {
        PriceRepository repo;
        private static string DefaultConnection = ConfigurationManager.ConnectionStrings["DangoteConnection"].ConnectionString;

        public PriceService()
        {
            var conn = new SqlConnection(DefaultConnection);
            var generator = new SqlGenerator<MyRow>(SqlProvider.MSSQL);
            repo = new PriceRepository(conn, generator);
        }

        public async Task<IEnumerable<MyRow>> GetAllAsync()
        {
            return await repo.FindAllAsync();
        }

        public async Task<bool> Add(MyRow row)
        {
            return await repo.InsertAsync(row);
        }

        public async Task<bool> Update(MyRow row)
        {
            return await repo.UpdateAsync(row);
        }
    }
}