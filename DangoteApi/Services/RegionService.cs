﻿using DangoteApi.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using MyRow = DangoteApi.Models.RegionsContext;

namespace DangoteApi.Services
{
    public class RegionService
    {
        RegionRepository repo;
        private static string DefaultConnection = ConfigurationManager.ConnectionStrings["DangoteConnection"].ConnectionString;

        public RegionService()
        {
            var conn = new SqlConnection(DefaultConnection);
            var generator = new SqlGenerator<MyRow>(SqlProvider.MSSQL);
            repo = new RegionRepository(conn, generator);
        }

        public async Task<IEnumerable<MyRow>> GetAllAsync()
        {
            return await repo.FindAllAsync();
        }
    }
}