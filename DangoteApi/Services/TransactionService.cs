﻿using DangoteApi.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using MyRow = DangoteApi.Models.TransactionsContext;

namespace DangoteApi.Services
{
    public class TransactionService
    {
        TransactionRepository repo;
        private static string DefaultConnection = ConfigurationManager.ConnectionStrings["DangoteConnection"].ConnectionString;

        public TransactionService()
        {
            var conn = new SqlConnection(DefaultConnection);
            var generator = new SqlGenerator<MyRow>(SqlProvider.MSSQL);
            repo = new TransactionRepository(conn, generator);
        }

        public IEnumerable<MyRow> GetAllAsync()
        {
            return repo.FindAll();
        }

        public IEnumerable<MyRow> GetAllUserAsync(string username)
        {
            return repo.FindAll(x => x.username == username);
        }

        public MyRow GetByIdAsync(string tran_id)
        {
            return repo.Find(x => x.tran_id == tran_id);
        }

        public bool Add(MyRow row)
        {
            return repo.Insert(row);
        }

        public bool Update(MyRow row)
        {
            return repo.Update(row);
        }
    }
}