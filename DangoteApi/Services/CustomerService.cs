﻿using DangoteApi.Repositories;
using MicroOrm.Dapper.Repositories.SqlGenerator;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using MyRow = DangoteApi.Models.CustomerContext;

namespace DangoteApi.Services
{
    public class CustomerService
    {
        CustomerRepository repo;
        private static string DefaultConnection = ConfigurationManager.ConnectionStrings["DangoteConnection"].ConnectionString;

        public CustomerService()
        {
            var conn = new SqlConnection(DefaultConnection);
            var generator = new SqlGenerator<MyRow>(SqlProvider.MSSQL);
            repo = new CustomerRepository(conn, generator);
        }

        public IEnumerable<MyRow> GetAllAsync()
        {
            return repo.FindAll();
        }

        public MyRow GetById(int Id)
        {
            return repo.Find(x => x.Id == Id);
        }

        public bool Add(MyRow row)
        {
            return repo.Insert(row);
        }

        public bool Update(MyRow row)
        {
            return repo.Update(row);
        }
    }
}